const client = require('./elasticsearch_client');

class LatestLocation {

  static async health() {
    return client.cluster.health();
  }

  static async createIndex() {
    return client.indices.create({
      index: 'latest_location',
      body: {
        settings: {
          number_of_shards: 1,
        },
        mappings: {
          location: {
            _source : { enabled : true },
            properties: {
              driverId: { type: 'integer' },
              category: { type: 'keyword'},
              coordinate: { type: 'geo_point'},
            }
          }
        }
      }
    })
  }

  static async deleteIndex() {
    return client.indices.delete({
      index: 'latest_location',
    });
  }

  static async indexDriverLocation(driver) {
    return client.index({
      index: 'latest_location',
      type: 'location',
      id: driver.driverId,
      refresh: true,
      body: {
        driverId: driver.driverId,
        category: driver.category,
        coordinate: {
          lat: driver.loc.lat,
          lon: driver.loc.lon,
        }
      }
    });
  }

  static async getLatestDriverLocation(driverId) {
    return client.get({
      index: 'latest_location',
      type: 'location',
      id: driverId,
      _source: true,
    });
  }

  static async searchDriverByCategory(category) {
    return client.search({
      index: 'latest_location',
      type: 'location',
      body: {
        query: {
          match: {
            category: category
          }
        }
      }
    });
  }

}

module.exports = LatestLocation;
